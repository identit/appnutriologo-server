# README #

This README documents general information about this repository, steps that are necessary to get the application up and running, additionally some development guidelines and info.

### What is this repository for? ###

This is the default Ruby on Rails (RoR) setup for a new project. This is the starting point for every project, it includes: RSpec, Guard, Bootstrap, Devise, and other Gems. It is already debugged and runs.

* Version 1.0
* Mexducando website: www.mexducando.com
* Lead Architect: saul.ortigoza@mexducando.com

### How do I get set up? ###

* Summary of set up: clone this repository
* Configuration: instal ruby > 2.0, rails, rake, and bundler gems.
* Dependencies: int the project dir, run bundle install
* Database configuration: for development it uses SQLite, and for production: MySQL
* How to run tests: write "bundle exec guard" or "rake" in cml
* Deployment instructions: NA yet

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin: saul.ortigoza@mexducando.com
* Other community or team contact: contacto@mexducando.com